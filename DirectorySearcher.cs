﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Delegates_HW
{
    internal class FileArgs : EventArgs
    {
        public string FileName { get; set; }
        public bool Cancel { get; set; }
        public FileArgs(string filename)
        {
            FileName = filename;
        }
    }

    internal class DirectorySearcher
    {
        public event EventHandler<FileArgs> OnFileFound;
        private string _directory;

        public DirectorySearcher(string directory)
        {
            _directory = directory;
        }

        public void Run()
        {
            RecursiveSearch(_directory);
        }

        private bool RecursiveSearch(string path)
        {
            var files = Directory.GetFiles(path);

            foreach (var file in files)
            {
                FileArgs args = new FileArgs(file);
                OnFileFound?.Invoke(this, args);
                if (args.Cancel)
                {
                    return false;
                }
            }

            var folders = Directory.GetDirectories(path);

            foreach( var folder in folders)
            {
                if(!RecursiveSearch(folder))
                    return false;
            }

            return true;
        }
    }
}
