﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Otus_Delegates_HW
{
    static class CollectionExtentions
    {
        public static T GetMax<T>(this IEnumerable e, Func<T, float> getParameter) where T : class
        {
            float maxValue = float.MinValue;
            T maxElement = null;

            foreach( T element in e)
            {
                var value = getParameter(element);

                if(value > maxValue)
                {
                    maxValue = value;
                    maxElement = element;
                }
            }

            return maxElement;
        }
    }
}
