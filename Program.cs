﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Delegates_HW
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<string> list = new List<string>()
            {
                "12",
                "11",
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9",
                "10"
            };
            Console.WriteLine("Коллекция строк:\n" + string.Join(" ", list));

            Console.WriteLine("Поиск максимального значения...");
            Console.WriteLine();
            Console.WriteLine(list.GetMax((string e) => { return float.Parse(e); }));


            Console.WriteLine();
            var location = Assembly.GetExecutingAssembly().Location;
            location = location.Substring(0, location.LastIndexOf('\\'));

            Console.WriteLine($"Создаем чекер для папки: {location}");
            var searcher = new DirectorySearcher(location);

            Console.WriteLine();

            searcher.OnFileFound += Searcher_OnFileFound;
            searcher.Run();

            Console.ReadLine();

        }

        private static void Searcher_OnFileFound(object sender, FileArgs args)
        {          
            Console.WriteLine($"Найден файл: {args.FileName}");
            Console.WriteLine("Продолжить? Y/N");
            var key = Console.ReadKey();
            Console.WriteLine();
            if(key.Key == ConsoleKey.N)
            {
                Console.WriteLine("Отменено.");
                args.Cancel = true;
            }
        }
    }
}
